#ifndef __CONFIG_H_
#define __CONFIG_H_

#include <stdio.h> 
#include <semaphore.h>	// sem
#include "DateTime.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _DEBUG 1
#define FWVersion 1.3

#define _BUILD_YEAR         	2017
#define _BUILD_MONTH           	6
#define _BUILD_DAY              23
#define _BUILD_HOUR            	9
#define _BUILD_MIN            	30
#define _BUILD_SEC           	0


#define cDefaultDataPath						"/media/data/DefaultDataFiles/"
#define cMFLDataPath							"/media/data/MFLDataFiles/"
#define cCaliperDataPath						"/media/data/CaliperDataFiles/"
#define cIMUDataPath							"/media/data/IMUDataFiles/"
#define cConfigFileName 						"config.txt"
#define cDefaultConfigFileFullPath				"/media/data/DefaultDataFiles/config.txt\0"

// Added instead cXxxxxPath
#define IMUDataPath								"/media/data/IMUDataFiles/"
#define CaliperDataPath							"/media/data/CaliperDataFiles/"
#define MFLDataPath								"/media/data/GeckoDataFiles/"
#define DefaultDataPath							"/media/data/DefaultDataFiles/"
#define	ConfigFilePath							"/media/data/"

#define MIN(a,b) a<b?a:b
#define MAX(a,b) a>b?a:b

#define MAXNBuffer  6 
#define MAXBufferSize  1*1024*1024

/********************************** Define hardware configuration *************************************/

#define MaxNArms 		22
#define MaxNOdometer 	3

#define sensoreMaxCount		7
#define sensorMaxPosition	5

/******************************************************************************************************/

#define FW_MINOR_VER_DECIMAL_NUMBER 10 // n = 1 One decimal minor version
#define MAX_DEVICE_CODE				3

/****************************************************************
 * Constants
 ****************************************************************/

// constants 
enum OdometerType {odoMagetic=0 /*M*/, odoPulse=1/*P*/, odoAnalog=2 /*A*/};
/****************************************************************
 * Struct (parameters in config.txt)
 ****************************************************************/
typedef unsigned char byte;
#define BYTE byte

extern 		const char *device_code[];

typedef enum
{
	LOM_IMU             =0,
	LOM_CALIPER			=1,
	LOM_MFL				=2,
	LOM_DEFAULT			=3
}LoggingOperationMode;

#define MAXLEN 							100

typedef struct config_parameters
{	
	// 1. Version info. 
	char	KeyFirmwareVersion							[MAXLEN];	float 			FirmwareVersion							;	
	char  	KeyCode 									[MAXLEN]; 	char 			Code [3]								;  		
	
	// 2. Date time info. 
	char 	KeyYear 									[MAXLEN];	unsigned short 	nYear									; 
	char 	KeyMonth									[MAXLEN]; 	byte			nMonth									;
	char  	KeyDay 										[MAXLEN];	byte			nDay									;
	char  	KeyHour 									[MAXLEN]; 	byte 			nHour									;	
	char  	KeyMinute									[MAXLEN]; 	byte 			nMinute									;
	char 	KeySecond									[MAXLEN]; 	byte 			nSecond									;
	
	// 3. Logging info.  
	char    KeyIsRelease								[MAXLEN];   byte 			IsRelease								;
	char 	KeyIsASCII									[MAXLEN];	byte 			IsASCII									;
	char 	KeyFrequency								[MAXLEN];	unsigned short	nFrequency								;
	char    KeyBufferCount 								[MAXLEN]; 	byte 			nBufferCount							;
	char 	KeyBufferSize								[MAXLEN]; 	unsigned int 	nBufferSize								;
	
	// 4. Terminal display and save file info. 
	char 	KeyIsPrintResult							[MAXLEN];  	byte 			IsPrintResult							;
	
	// 5. pig detils
	char 	KeyBodyDiameterInMM 						[MAXLEN];	float  			BodyDiameterInMM						;	
	char 	KeyOdometerCount							[MAXLEN];	byte 			nOdometerCount							;
	char  	KeyOdometerType								[MAXLEN];	byte 			nOdometerType 							;
	char 	KeyOdometerDiameterInMM						[MAXLEN];	float 			OdometerDiameterInMM					;
	char  	KeyOdometerPulsePerRevolution 				[MAXLEN]; 	byte 			nOdometerPulsePerRevolution				; 
	
	// 6. caliper specific information
	char 	KeyCaliperArmCount 							[MAXLEN];	unsigned short 	nCaliperArmCount						;
	char  	KeyCaliperArmLenghtInMM						[MAXLEN];	float 			CaliperArmLenghtInMM					;
	char 	KeyCaliperArmCenterOffsetInMM				[MAXLEN];  	float 			CaliperArmCenterOffsetInMM				;
		
	// 7. MFL specific information	
	char    KeyMFLBoardCount							[MAXLEN];   unsigned short 	nMFLBoardCount							;
	char    KeyMFLADCChannelCount						[MAXLEN];   unsigned short 	nMFLADCChannelCount						;
	char    KeyMFLMUXChannelCount						[MAXLEN];   unsigned short 	nMFLMUXChannelCount						;

	// 8. caliper calibration information
	char    KeyCaliperCalMaskBits						[MAXLEN];   unsigned short 	nCaliperCalMaskBits						;
	char    KeyCaliperCalParamsMAX						[MAXLEN];   unsigned short 	nCaliperCalParamsMAX[MaxNArms]			;
	char    KeyCaliperCalParamsMIN						[MAXLEN];   unsigned short 	nCaliperCalParamsMIN[MaxNArms]			;
	char    KeyCaliperCalDiaMAX							[MAXLEN];   float 			nCaliperCalDiaMAX						;
	char    KeyCaliperCalDiaMIN							[MAXLEN];   float 			nCaliperCalDiaMIN						;

	char    KeyCaliperCalParamsEXT1						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT1[MaxNArms]			;
	char    KeyCaliperCalDiaEXT1						[MAXLEN];   float 			nCaliperCalDiaEXT1						;

	char    KeyCaliperCalParamsEXT2						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT2[MaxNArms]			;
	char    KeyCaliperCalDiaEXT2						[MAXLEN];   float 			nCaliperCalDiaEXT2						;

	char    KeyCaliperCalParamsEXT3						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT3[MaxNArms]			;
	char    KeyCaliperCalDiaEXT3						[MAXLEN];   float 			nCaliperCalDiaEXT3						;

	char    KeyCaliperCalParamsEXT4						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT4[MaxNArms]			;
	char    KeyCaliperCalDiaEXT4						[MAXLEN];   float 			nCaliperCalDiaEXT4						;

	char    KeyCaliperCalParamsEXT5						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT5[MaxNArms]			;
	char    KeyCaliperCalDiaEXT5						[MAXLEN];   float 			nCaliperCalDiaEXT5						;

	char    KeyCaliperCalParamsEXT6						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT6[MaxNArms]			;
	char    KeyCaliperCalDiaEXT6						[MAXLEN];   float 			nCaliperCalDiaEXT6						;

	char    KeyCaliperCalParamsEXT7						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT7[MaxNArms]			;
	char    KeyCaliperCalDiaEXT7						[MAXLEN];   float 			nCaliperCalDiaEXT7						;

	char    KeyCaliperCalParamsEXT8						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT8[MaxNArms]			;
	char    KeyCaliperCalDiaEXT8						[MAXLEN];   float 			nCaliperCalDiaEXT8						;

	char    KeyCaliperCalParamsEXT9						[MAXLEN];   unsigned short 	nCaliperCalParamsEXT9[MaxNArms]			;
	char    KeyCaliperCalDiaEXT9						[MAXLEN];   float 			nCaliperCalDiaEXT9						;

	char    KeyCaliperCalParamsEXT10					[MAXLEN];   unsigned short 	nCaliperCalParamsEXT10[MaxNArms]		;
	char    KeyCaliperCalDiaEXT10						[MAXLEN];   float 			nCaliperCalDiaEXT10						;
} CONFIGURATION;

//const CONFIGURATION cDefaultConfig;

extern CONFIGURATION m_Config;
extern char    DirPath[200];
extern char	   *ConfigFileName;

#if 0
extern void  LoadConfiguration(CONFIGURATION* config);
#endif

void  parseConfiguration(char *FileName, CONFIGURATION* config, LoggingOperationMode device);
void  saveConfiguration(char *FileName, CONFIGURATION*config, LoggingOperationMode device);
void  printConfiguration(CONFIGURATION*config, FILE * target, LoggingOperationMode device);

int   isFileExists (char *filename);
void  uppercase( char *sPtr );

void device_config_intial(void);
void device_config_reintial(void);

#ifdef __cplusplus
}
#endif

#endif
