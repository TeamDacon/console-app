#ifndef CAL_FILEFORMAT_H__
#define CAL_FILEFORMAT_H__

#include <stdint.h>
#include "config.h"

	#define MaxNArms 		22
	// http://www.codeproject.com/Articles/48810/Structure-Padding
	#pragma pack(push)  /* push current alignment to stack */
	#pragma pack(1)     /* set alignment to 1 byte boundary */

	// http://www.binaryconvert.com/convert_signed_short.html
	typedef struct tagHEADERINFO
	{
		BYTE			FirmwareVersion												;
		BYTE			FirmwareSubVersion											;
		BYTE			FirmwareDate												;
		BYTE			FirmwareMonth												;
		unsigned short	FirmwareYear												;
		BYTE			OperationCode[3]											;			// CAL
		unsigned short	SamplingRateInHz											;			// 0-16k
		BYTE			NumberOfVariableGroups										;			// 1...16
		float			OdometerDiameterInMM										;
		float			InternalPipeDiameterInMM									; 			
		BYTE			NumberOfOdometers											;

		// \Added By Sutipong
		unsigned int	HeaderSizeInByte											;	///<if 0 = 512 x 20 pages, otherwise 512 x Reserved page
		BYTE			FirmwareRevision;	///<no use this time
		BYTE			nOdometerType;	///<Odometer type : pulse, magnet
	} HEADERINFO;

	typedef struct tagVARIABLEGROUPINFO
	{
		unsigned short	NumberOfVariables											;			// 2
		BYTE			DataSizeInBytePerVariable									;			// 1
		BYTE			isSignedData												;			// 1
		BYTE			isRealNumber												;			// 1
		double			ScaleFactor													;
		byte			Filler00[32-5-8]											;
	} VARIABLEGROUPINFO;

	typedef struct tagCAL_EXTRA_PARAMS
	{
		unsigned short		CalExt[MaxNArms];
		float				DiaExt;
	}CAL_EXTRA_PARAMS;

	typedef struct tagCALIPER_CAL_PARAMS_MINMAX
	{
		unsigned short			CalMaskBits;					//Mask bits of caliper calibration is used
		unsigned short			CalParamMax[MaxNArms];			//Caliper array arms data (MAX)
		unsigned short			CalParamMin[MaxNArms];			//Caliper array arms data (MIN)
		float					DiaMax;							//Diameter (MAX)
		float					DiaMin;							//Diameter (MIN)
	}CALIPER_CAL_PARAMS_MINMAX;

	typedef struct tagCALIPER_CAL_PARAMS_EXT
	{
		CAL_EXTRA_PARAMS		ParamsExt1;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt2;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt3;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt4;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt5;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt6;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt7;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt8;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt9;						//Extra parameters for calibration
		CAL_EXTRA_PARAMS		ParamsExt10;					//Extra parameters for calibration
	}CALIPER_CAL_PARAMS_EXT;

	typedef struct tagCALIPERINFO
	{
		float				ArmLengthInMM												;
		float				BaseHeightInMM												; 
		float				BaseToArmCenterInMM											; 
	} CALIPERINFO;

	// Added By Sutipong for ART  project
	typedef struct tagIMUINFO
	{
		uint16_t		SamplingRateInHz;	// 0-16k //
	}IMUINFO;

	typedef struct tagFILEHEADER_CALIPER
	{
		HEADERINFO				HeaderInfo											;
		BYTE					Filler01[512-sizeof(HEADERINFO)]					;

		VARIABLEGROUPINFO		VariableGroupInfo[8]								;									// 0...7 maximum 8 group
		BYTE					Filler02[512 - 8 * sizeof(VARIABLEGROUPINFO)]		;

		CALIPERINFO				CaliperInfo											;
		BYTE					Filler03[512 - sizeof(CALIPERINFO)]					;

		// IMU page 4
		BYTE					Filler04[512]										;

		// Calibration Parameter page 5
		CALIPER_CAL_PARAMS_MINMAX	CaliperCalParam										;
		BYTE						Filler05[512 - sizeof(CALIPER_CAL_PARAMS_MINMAX)]	;

		// Calibration Parameter Extra page 6
		CALIPER_CAL_PARAMS_EXT	CaliperCalParamExt									;
		BYTE					Filler06[512 - sizeof(CALIPER_CAL_PARAMS_EXT)]		;

		// Reserved page 7
		BYTE					Filler07[512]										;

		// Reserved page 8
		BYTE					Filler08[512]										;

		// Reserved page 9
		BYTE					Filler09[512]										;

		// Reserved page 10
		BYTE					Filler10[512]											;

		// Bank page 11
		BYTE					Bank11[512]											;

		// Bank page 12
		BYTE					Bank12[512]											;

		// Bank page 13
		BYTE					Bank13[512]											;

		// Bank page 14
		BYTE					Bank14[512]											;

		// Bank page 15
		BYTE					Bank15[512]											;

		// Bank page 16
		BYTE					Bank16[512]											;

		// Bank page 17
		BYTE					Bank17[512]											;

		// Bank page 18
		BYTE					Bank18[512]											;

		// Bank page 19
		BYTE					Bank19[512]											;

		// Bank page 20
		BYTE					Bank20[512]											;
	} FILEHEADER_CALIPER;

	#pragma pack(pop)   /* restore original alignment from stack */

#endif // CAL_FILEFORMAT_H__

