#include <ctype.h>
#include <fcntl.h> 
#include <stdio.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <string.h> 
#include <time.h> 
#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include "config.h"
#include "system.h"

static char  ConfigPath[200];
char  		 DirPath[200];

char	*ConfigFileName;

//const CONFIGURATION cDefaultConfig=
//{
//	// 1. Version info.
//	.KeyFirmwareVersion 									= "FW",									.FirmwareVersion 					= FWVersion				,
//	.KeyCode 												= "CODE",								.Code 								= "IMU"					,
//
//	// 2. Date time info.
//	.KeyYear 												= "RTC_YEAR", 							.nYear 								= 2016					,
//	.KeyMonth												= "RTC_MONTH",							.nMonth								= 1						,
//	.KeyDay													= "RTC_DOM",  							.nDay								= 30					,
//	.KeyHour 												= "RTC_HOUR",							.nHour 								= 22					,
//	.KeyMinute 												= "RTC_MIN",							.nMinute 							= 22					,
//	.KeySecond 												= "RTC_SEC",							.nSecond 							= 22					,
//
//	// 3. Logging info.
//	.KeyIsRelease											= "IsRelease",   						.IsRelease							= 0						,
//	.KeyIsASCII 											= "ASCII",								.IsASCII							= 1						,
//	.KeyFrequency											= "Frequency",							.nFrequency							= 50					,
//	.KeyBufferCount											= "LogBufferCount",						.nBufferCount						= 6						,
//	.KeyBufferSize											= "LogBufferSizeInByte",				.nBufferSize						= 16 * 1024				,
//
//	// 4. Terminal display and save file info.
//	.KeyIsPrintResult										= "PrintToTerminal", 					.IsPrintResult						= 1						,
//
//	// 5. pig detils
//	.KeyBodyDiameterInMM 									= "Pig_BodyDiameterInMM",				.BodyDiameterInMM					= 25.2					,
//	.KeyOdometerCount										= "Odometer_Count", 					.nOdometerCount						= 3						,
//	.KeyOdometerDiameterInMM								= "Odometer_DiameterInMM", 				.OdometerDiameterInMM 				= 20.5					,
//	.KeyOdometerType										= "Odometer_Type", 						.nOdometerType 						= odoMagetic			,
//	.KeyOdometerPulsePerRevolution							= "Odometer_PulsePerRevolution",		.nOdometerPulsePerRevolution		= 8						,
//
//	// 6. caliper specific information
//	.KeyCaliperArmCount 									= "Caliper_ArmCount",					.nCaliperArmCount					= 10				 	,
//	.KeyCaliperArmLenghtInMM								= "Caliper_ArmLenghtInMM",				.CaliperArmLenghtInMM				= 120					,
//	.KeyCaliperArmCenterOffsetInMM							= "Caliper_ArmCenterOffsetInMM",		.CaliperArmCenterOffsetInMM			= 12.0					,
//
//	// 7. MFL specific information
//	.KeyMFLBoardCount										= "MFL_BoardCount", 					.nMFLBoardCount						= 1 					,
//	.KeyMFLADCChannelCount									= "MFL_ADCChannelCount",   				.nMFLADCChannelCount				= 8						,
//	.KeyMFLMUXChannelCount									= "MFL_MUXChannelCount",   				.nMFLMUXChannelCount				= 8						,
//
//	// 8. caliper calibration information
//	.KeyCaliperCalMaskBits									= "Caliper_CalMaskBits",   				.nCaliperCalMaskBits				= 0x0000				,
//	.KeyCaliperCalParamsMAX									= "Caliper_CalParamsMAX", 				.nCaliperCalParamsMAX				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalParamsMIN									= "Caliper_CalParamsMIN",   			.nCaliperCalParamsMIN				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}					,
//	.KeyCaliperCalDiaMAX									= "Caliper_CalDiaMAX",   				.nCaliperCalDiaMAX					= 0.0						,
//	.KeyCaliperCalDiaMIN									= "Caliper_CalDiaMIN",   				.nCaliperCalDiaMIN					= 0.0						,
//
//	.KeyCaliperCalParamsEXT1								= "Caliper_CalParamsEXT1", 				.nCaliperCalParamsEXT1				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT1									= "Caliper_CalDiaEXT1",   				.nCaliperCalDiaEXT1					= 0.0						,
//
//	.KeyCaliperCalParamsEXT2								= "Caliper_CalParamsEXT2", 				.nCaliperCalParamsEXT2				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT2									= "Caliper_CalDiaEXT2",   				.nCaliperCalDiaEXT2					= 0.0						,
//
//	.KeyCaliperCalParamsEXT3								= "Caliper_CalParamsEXT3", 				.nCaliperCalParamsEXT3				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT3									= "Caliper_CalDiaEXT3",   				.nCaliperCalDiaEXT3					= 0.0						,
//
//	.KeyCaliperCalParamsEXT4								= "Caliper_CalParamsEXT4", 				.nCaliperCalParamsEXT4				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT4									= "Caliper_CalDiaEXT4",   				.nCaliperCalDiaEXT4					= 0.0						,
//
//	.KeyCaliperCalParamsEXT5								= "Caliper_CalParamsEXT5", 				.nCaliperCalParamsEXT5				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT5									= "Caliper_CalDiaEXT5",   				.nCaliperCalDiaEXT5					= 0.0						,
//
//	.KeyCaliperCalParamsEXT6								= "Caliper_CalParamsEXT6", 				.nCaliperCalParamsEXT6				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT6									= "Caliper_CalDiaEXT6",   				.nCaliperCalDiaEXT6					= 0.0						,
//
//	.KeyCaliperCalParamsEXT7								= "Caliper_CalParamsEXT7", 				.nCaliperCalParamsEXT7				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT7									= "Caliper_CalDiaEXT7",   				.nCaliperCalDiaEXT7					= 0.0						,
//
//	.KeyCaliperCalParamsEXT8								= "Caliper_CalParamsEXT8", 				.nCaliperCalParamsEXT8				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT8									= "Caliper_CalDiaEXT8",   				.nCaliperCalDiaEXT8					= 0.0						,
//
//	.KeyCaliperCalParamsEXT9								= "Caliper_CalParamsEXT9", 				.nCaliperCalParamsEXT9				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT9									= "Caliper_CalDiaEXT9",   				.nCaliperCalDiaEXT9					= 0.0						,
//
//	.KeyCaliperCalParamsEXT10								= "Caliper_CalParamsEXT10", 			.nCaliperCalParamsEXT10				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
//	.KeyCaliperCalDiaEXT10									= "Caliper_CalDiaEXT10",   				.nCaliperCalDiaEXT10				= 0.0						,
//};

CONFIGURATION m_Config=
{
	// 1. Version info.
	.KeyFirmwareVersion 									= "FW",									.FirmwareVersion 					= FWVersion				,
	.KeyCode 												= "CODE",								.Code 								= "IMU"					,

	// 2. Date time info.
	.KeyYear 												= "RTC_YEAR", 							.nYear 								= 2016					,
	.KeyMonth												= "RTC_MONTH",							.nMonth								= 1						,
	.KeyDay													= "RTC_DOM",  							.nDay								= 30					,
	.KeyHour 												= "RTC_HOUR",							.nHour 								= 22					,
	.KeyMinute 												= "RTC_MIN",							.nMinute 							= 22					,
	.KeySecond 												= "RTC_SEC",							.nSecond 							= 22					,

	// 3. Logging info.
	.KeyIsRelease											= "IsRelease",   						.IsRelease							= 0						,
	.KeyIsASCII 											= "ASCII",								.IsASCII							= 1						,
	.KeyFrequency											= "Frequency",							.nFrequency							= 50					,
	.KeyBufferCount											= "LogBufferCount",						.nBufferCount						= 6						,
	.KeyBufferSize											= "LogBufferSizeInByte",				.nBufferSize						= 16 * 1024				,

	// 4. Terminal display and save file info.
	.KeyIsPrintResult										= "PrintToTerminal", 					.IsPrintResult						= 1						,

	// 5. pig detils
	.KeyBodyDiameterInMM 									= "Pig_BodyDiameterInMM",				.BodyDiameterInMM					= 25.2					,
	.KeyOdometerCount										= "Odometer_Count", 					.nOdometerCount						= 3						,
	.KeyOdometerDiameterInMM								= "Odometer_DiameterInMM", 				.OdometerDiameterInMM 				= 20.5					,
	.KeyOdometerType										= "Odometer_Type", 						.nOdometerType 						= odoMagetic			,
	.KeyOdometerPulsePerRevolution							= "Odometer_PulsePerRevolution",		.nOdometerPulsePerRevolution		= 8						,

	// 6. caliper specific information
	.KeyCaliperArmCount 									= "Caliper_ArmCount",					.nCaliperArmCount					= 10				 	,
	.KeyCaliperArmLenghtInMM								= "Caliper_ArmLenghtInMM",				.CaliperArmLenghtInMM				= 120					,
	.KeyCaliperArmCenterOffsetInMM							= "Caliper_ArmCenterOffsetInMM",		.CaliperArmCenterOffsetInMM			= 12.0					,

	// 7. MFL specific information
	.KeyMFLBoardCount										= "MFL_BoardCount", 					.nMFLBoardCount						= 1 					,
	.KeyMFLADCChannelCount									= "MFL_ADCChannelCount",   				.nMFLADCChannelCount				= 8						,
	.KeyMFLMUXChannelCount									= "MFL_MUXChannelCount",   				.nMFLMUXChannelCount				= 8						,

	// 8. caliper calibration information
	.KeyCaliperCalMaskBits									= "Caliper_CalMaskBits",   				.nCaliperCalMaskBits				= 0x0000				,
	.KeyCaliperCalParamsMAX									= "Caliper_CalParamsMAX", 				.nCaliperCalParamsMAX				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalParamsMIN									= "Caliper_CalParamsMIN",   			.nCaliperCalParamsMIN				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}					,
	.KeyCaliperCalDiaMAX									= "Caliper_CalDiaMAX",   				.nCaliperCalDiaMAX					= 0.0						,
	.KeyCaliperCalDiaMIN									= "Caliper_CalDiaMIN",   				.nCaliperCalDiaMIN					= 0.0						,

	.KeyCaliperCalParamsEXT1								= "Caliper_CalParamsEXT1", 				.nCaliperCalParamsEXT1				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT1									= "Caliper_CalDiaEXT1",   				.nCaliperCalDiaEXT1					= 0.0						,

	.KeyCaliperCalParamsEXT2								= "Caliper_CalParamsEXT2", 				.nCaliperCalParamsEXT2				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT2									= "Caliper_CalDiaEXT2",   				.nCaliperCalDiaEXT2					= 0.0						,

	.KeyCaliperCalParamsEXT3								= "Caliper_CalParamsEXT3", 				.nCaliperCalParamsEXT3				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT3									= "Caliper_CalDiaEXT3",   				.nCaliperCalDiaEXT3					= 0.0						,

	.KeyCaliperCalParamsEXT4								= "Caliper_CalParamsEXT4", 				.nCaliperCalParamsEXT4				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT4									= "Caliper_CalDiaEXT4",   				.nCaliperCalDiaEXT4					= 0.0						,

	.KeyCaliperCalParamsEXT5								= "Caliper_CalParamsEXT5", 				.nCaliperCalParamsEXT5				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT5									= "Caliper_CalDiaEXT5",   				.nCaliperCalDiaEXT5					= 0.0						,

	.KeyCaliperCalParamsEXT6								= "Caliper_CalParamsEXT6", 				.nCaliperCalParamsEXT6				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT6									= "Caliper_CalDiaEXT6",   				.nCaliperCalDiaEXT6					= 0.0						,

	.KeyCaliperCalParamsEXT7								= "Caliper_CalParamsEXT7", 				.nCaliperCalParamsEXT7				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT7									= "Caliper_CalDiaEXT7",   				.nCaliperCalDiaEXT7					= 0.0						,

	.KeyCaliperCalParamsEXT8								= "Caliper_CalParamsEXT8", 				.nCaliperCalParamsEXT8				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT8									= "Caliper_CalDiaEXT8",   				.nCaliperCalDiaEXT8					= 0.0						,

	.KeyCaliperCalParamsEXT9								= "Caliper_CalParamsEXT9", 				.nCaliperCalParamsEXT9				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT9									= "Caliper_CalDiaEXT9",   				.nCaliperCalDiaEXT9					= 0.0						,

	.KeyCaliperCalParamsEXT10								= "Caliper_CalParamsEXT10", 			.nCaliperCalParamsEXT10				= {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0} 					,
	.KeyCaliperCalDiaEXT10									= "Caliper_CalDiaEXT10",   				.nCaliperCalDiaEXT10				= 0.0						,
};

const char *device_code[] =
{
		"IMU",
		"CAL",
		"MFL",
};


void _LoadConfiguration(char *FileName, CONFIGURATION* config, LoggingOperationMode device)
{
	config->nYear 	= _BUILD_YEAR	;
	config->nMonth	= _BUILD_MONTH	;
	config->nDay	= _BUILD_DAY	;
	config->nHour	= _BUILD_HOUR	;
	config->nMinute	= _BUILD_MIN	;
	config->nSecond	= _BUILD_SEC	;

	if (isFileExists (FileName))
	{
		parseConfiguration(FileName, config, device);
	}

	if (_DEBUG)
	{
		printf("\n");
		printf("+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+");
		printf("\n");
		printf("|                                                                   CONFIGURATION PARAMETERS                                                                                     |");
		printf("\n");
		printf("+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+");
		printf("\n");
		printConfiguration (config, stdout, device);
		printf("+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+");
		printf("\n");
		printf("\n");
	}

	saveConfiguration(FileName, config, device);
}


#define EXTPARAMS(num) do{ \
	fprintf(target, "%-50s %s "					, config->KeyCaliperCalParamsEXT##num, 				mySymbol); \
	fprintf(target, "{ "); \
	for(i=0; i<MaxNArms; i++) \
	{ \
		fprintf(target, "%d", config->nCaliperCalParamsEXT##num[i]); \
		if(i<MaxNArms-1) \
		fprintf(target, ","); \
		fprintf(target, " "); \
	} \
	fprintf(target, "} \n"); \
	fprintf(target, "%-50s %s %.2lf\n"				, config->KeyCaliperCalDiaEXT##num, 			mySymbol, config->nCaliperCalDiaEXT##num); \
} while(0)

#define PARSE_EXTPARAMS(num) do{ \
		if			(!strcmp(fName, config->KeyCaliperCalParamsEXT##num))									parseMultiInt(fValue, config->nCaliperCalParamsEXT##num); \
		else if		(!strcmp(fName, config->KeyCaliperCalDiaEXT##num))										config->nCaliperCalDiaEXT##num				= atof(fValue);	\
} while(0)

//-------------------------------------------------------------------------------------------
void printIMUConfiguration(CONFIGURATION*config, FILE *target)
{
	int i;

	char mySymbol[5]="=\0";

	// 1. Version info.
	fprintf(target, "%-50s %s %.1lf\n"			, config->KeyFirmwareVersion, 					mySymbol, config->FirmwareVersion);
	fprintf(target, "%-50s %s %c%c%c\n"			, config->KeyCode, 								mySymbol, config->Code[0],config->Code[1],config->Code[2]);

	// 2. Date time info.
	fprintf(target, "%-50s %s %d\n"				, config->KeyYear, 								mySymbol, config->nYear);
	fprintf(target, "%-50s %s %d\n"				, config->KeyMonth, 							mySymbol, config->nMonth);
	fprintf(target, "%-50s %s %d\n"				, config->KeyDay, 								mySymbol, config->nDay);
	fprintf(target, "%-50s %s %d\n"				, config->KeyHour, 								mySymbol, config->nHour);
	fprintf(target, "%-50s %s %d\n"				, config->KeyMinute, 							mySymbol, config->nMinute);
	fprintf(target, "%-50s %s %d\n"				, config->KeySecond, 							mySymbol, config->nSecond);

	// 3. Logging info.
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsRelease, 						mySymbol, config->IsRelease?'Y':'N');
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsASCII, 							mySymbol, config->IsASCII?'Y': 'N');
	fprintf(target, "%-50s %s %d\n"				, config->KeyFrequency, 						mySymbol, config->nFrequency);
	fprintf(target, "%-50s %s %d\n"				, config->KeyBufferCount, 						mySymbol, config->nBufferCount);
	fprintf(target, "%-50s %s %d\n"				, config->KeyBufferSize, 						mySymbol, config->nBufferSize);

	// 4. Terminal display and save file info.
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsPrintResult, 					mySymbol, config->IsPrintResult?'Y': 'N');

	// 5. pig detils
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyBodyDiameterInMM, 					mySymbol, config->BodyDiameterInMM);
	fprintf(target, "%-50s %s %d\n"				, config->KeyOdometerCount,  					mySymbol, config->nOdometerCount);
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyOdometerDiameterInMM,  			mySymbol, config->OdometerDiameterInMM);
	fprintf(target, "%-50s %s %c\n"				, config->KeyOdometerType, 						mySymbol, config->nOdometerType==odoMagetic?'M':
																										  config->nOdometerType==odoPulse?'P' :'A' );

	fprintf(target, "%-50s %s %d\n"				, config->KeyOdometerPulsePerRevolution,		mySymbol, config->nOdometerPulsePerRevolution);

}

void printCaliperConfiguration(CONFIGURATION*config, FILE *target)
{
	int i;

	char mySymbol[5]="=\0";	
	
	// 1. Version info.
	fprintf(target, "%-50s %s %.1lf\n"			, config->KeyFirmwareVersion, 					mySymbol, config->FirmwareVersion);
	fprintf(target, "%-50s %s %c%c%c\n"			, config->KeyCode, 								mySymbol, config->Code[0],config->Code[1],config->Code[2]);
	
	// 2. Date time info. 
	fprintf(target, "%-50s %s %d\n"				, config->KeyYear, 								mySymbol, config->nYear);		
	fprintf(target, "%-50s %s %d\n"				, config->KeyMonth, 							mySymbol, config->nMonth);
	fprintf(target, "%-50s %s %d\n"				, config->KeyDay, 								mySymbol, config->nDay);				                                                    								
	fprintf(target, "%-50s %s %d\n"				, config->KeyHour, 								mySymbol, config->nHour);
	fprintf(target, "%-50s %s %d\n"				, config->KeyMinute, 							mySymbol, config->nMinute);
	fprintf(target, "%-50s %s %d\n"				, config->KeySecond, 							mySymbol, config->nSecond);		
	
	// 3. Logging info.	
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsRelease, 						mySymbol, config->IsRelease?'Y':'N'); 
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsASCII, 							mySymbol, config->IsASCII?'Y': 'N');	
	fprintf(target, "%-50s %s %d\n"				, config->KeyFrequency, 						mySymbol, config->nFrequency); 		
	fprintf(target, "%-50s %s %d\n"				, config->KeyBufferCount, 						mySymbol, config->nBufferCount); 		
	fprintf(target, "%-50s %s %d\n"				, config->KeyBufferSize, 						mySymbol, config->nBufferSize); 	
	
	// 4. Terminal display and save file info.  
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsPrintResult, 					mySymbol, config->IsPrintResult?'Y': 'N');		

	// 5. pig detils
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyBodyDiameterInMM, 					mySymbol, config->BodyDiameterInMM); 		
	fprintf(target, "%-50s %s %d\n"				, config->KeyOdometerCount,  					mySymbol, config->nOdometerCount); 		
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyOdometerDiameterInMM,  			mySymbol, config->OdometerDiameterInMM); 
	fprintf(target, "%-50s %s %c\n"				, config->KeyOdometerType, 						mySymbol, config->nOdometerType==odoMagetic?'M':
																										  config->nOdometerType==odoPulse?'P' :'A' );

	fprintf(target, "%-50s %s %d\n"				, config->KeyOdometerPulsePerRevolution,		mySymbol, config->nOdometerPulsePerRevolution);
	
	// 6. caliper specific information	
	fprintf(target, "%-50s %s %d\n"				, config->KeyCaliperArmCount, 					mySymbol, config->nCaliperArmCount); 
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyCaliperArmLenghtInMM, 				mySymbol, config->CaliperArmLenghtInMM); 			
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyCaliperArmCenterOffsetInMM, 		mySymbol, config->CaliperArmCenterOffsetInMM); 
	
	// 7. MFL specific information	
	fprintf(target, "%-50s %s %d\n"				, config->KeyMFLBoardCount, 					mySymbol, config->nMFLBoardCount); 
	fprintf(target, "%-50s %s %d\n"				, config->KeyMFLADCChannelCount, 				mySymbol, config->nMFLADCChannelCount); 
	fprintf(target, "%-50s %s %d\n"				, config->KeyMFLMUXChannelCount, 				mySymbol, config->nMFLMUXChannelCount); 

	// 8. caliper calibration information
	fprintf(target, "%-50s %s %04x\n"			, config->KeyCaliperCalMaskBits, 				mySymbol, config->nCaliperCalMaskBits);

	fprintf(target, "%-50s %s "					, config->KeyCaliperCalParamsMAX, 				mySymbol);
	fprintf(target, "{ ");
	for(i=0; i<MaxNArms; i++)
	{
		fprintf(target, "%d", config->nCaliperCalParamsMAX[i]);

		if(i<MaxNArms-1)
		fprintf(target, ",");

		fprintf(target, " ");
	}
	fprintf(target, "} \n");

	fprintf(target, "%-50s %s "					, config->KeyCaliperCalParamsMIN, 				mySymbol);
	fprintf(target, "{ ");
	for(i=0; i<MaxNArms; i++)
	{
		fprintf(target, "%d", config->nCaliperCalParamsMIN[i]);

		if(i<MaxNArms-1)
		fprintf(target, ",");

		fprintf(target, " ");
	}
	fprintf(target, "} \n");

	fprintf(target, "%-50s %s %.2lf\n"				, config->KeyCaliperCalDiaMAX, 					mySymbol, config->nCaliperCalDiaMAX);
	fprintf(target, "%-50s %s %.2lf\n"				, config->KeyCaliperCalDiaMIN, 					mySymbol, config->nCaliperCalDiaMIN);

	//Extra calibration parameter
	EXTPARAMS(1);
	EXTPARAMS(2);
	EXTPARAMS(3);
	EXTPARAMS(4);
	EXTPARAMS(5);
	EXTPARAMS(6);
	EXTPARAMS(7);
	EXTPARAMS(8);
	EXTPARAMS(9);
	EXTPARAMS(10);
}

void printMFLConfiguration(CONFIGURATION*config, FILE *target)
{
	int i;

	char mySymbol[5]="=\0";

	// 1. Version info.
	fprintf(target, "%-50s %s %.1lf\n"			, config->KeyFirmwareVersion, 					mySymbol, config->FirmwareVersion);
	fprintf(target, "%-50s %s %c%c%c\n"			, config->KeyCode, 								mySymbol, config->Code[0],config->Code[1],config->Code[2]);

	// 2. Date time info.
	fprintf(target, "%-50s %s %d\n"				, config->KeyYear, 								mySymbol, config->nYear);
	fprintf(target, "%-50s %s %d\n"				, config->KeyMonth, 							mySymbol, config->nMonth);
	fprintf(target, "%-50s %s %d\n"				, config->KeyDay, 								mySymbol, config->nDay);
	fprintf(target, "%-50s %s %d\n"				, config->KeyHour, 								mySymbol, config->nHour);
	fprintf(target, "%-50s %s %d\n"				, config->KeyMinute, 							mySymbol, config->nMinute);
	fprintf(target, "%-50s %s %d\n"				, config->KeySecond, 							mySymbol, config->nSecond);

	// 3. Logging info.
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsRelease, 						mySymbol, config->IsRelease?'Y':'N');
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsASCII, 							mySymbol, config->IsASCII?'Y': 'N');
	fprintf(target, "%-50s %s %d\n"				, config->KeyFrequency, 						mySymbol, config->nFrequency);
	fprintf(target, "%-50s %s %d\n"				, config->KeyBufferCount, 						mySymbol, config->nBufferCount);
	fprintf(target, "%-50s %s %d\n"				, config->KeyBufferSize, 						mySymbol, config->nBufferSize);

	// 4. Terminal display and save file info.
	fprintf(target, "%-50s %s %c\n"				, config->KeyIsPrintResult, 					mySymbol, config->IsPrintResult?'Y': 'N');

	// 5. pig detils
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyBodyDiameterInMM, 					mySymbol, config->BodyDiameterInMM);
	fprintf(target, "%-50s %s %d\n"				, config->KeyOdometerCount,  					mySymbol, config->nOdometerCount);
	fprintf(target, "%-50s %s %.2lf\n"			, config->KeyOdometerDiameterInMM,  			mySymbol, config->OdometerDiameterInMM);
	fprintf(target, "%-50s %s %c\n"				, config->KeyOdometerType, 						mySymbol, config->nOdometerType==odoMagetic?'M':
																										  config->nOdometerType==odoPulse?'P' :'A' );

	fprintf(target, "%-50s %s %d\n"				, config->KeyOdometerPulsePerRevolution,		mySymbol, config->nOdometerPulsePerRevolution);

	// 7. MFL specific information
	fprintf(target, "%-50s %s %d\n"				, config->KeyMFLBoardCount, 					mySymbol, config->nMFLBoardCount);
	fprintf(target, "%-50s %s %d\n"				, config->KeyMFLADCChannelCount, 				mySymbol, config->nMFLADCChannelCount);
	fprintf(target, "%-50s %s %d\n"				, config->KeyMFLMUXChannelCount, 				mySymbol, config->nMFLMUXChannelCount);

}

void  printConfiguration(CONFIGURATION *config, FILE *target, LoggingOperationMode device)
{
	switch (device)
	{
		case LOM_IMU:
			printIMUConfiguration(config, target);
			break;

		case LOM_CALIPER:
			printCaliperConfiguration(config, target);
			break;

		case LOM_MFL:
			printMFLConfiguration(config, target);
			break;

		default:
			break;
	}
}

void parseMultiInt(char* str, unsigned short *parser)
{
	int i;

	if(str[0]=='{')
	{
		str+=1;

		for(i=0; i<MaxNArms && str!=NULL; i++)
		{

			if(i==0) str  = strtok (str, " ,");
			else str  = strtok (NULL, " ,");

			if(str==NULL)
				break;

			parser[i] = atoi(str);
		}
	}
}

//-------------------------------------------------------------------------------------------
void parseIMUConfiguration(char *FileName, CONFIGURATION* config)
{
	FILE *h_File ;
	char fName [80], fSymbol[10], fValue [200];

	int i;
	h_File = fopen(FileName, "r");

	if (h_File)
	{
		while (!feof(h_File))
		{
			fscanf(h_File, "%s %s %200[^\n]", fName, fSymbol, fValue);

			// 1. Version info.
			if 			(!strcmp(fName, config->KeyFirmwareVersion))										config->FirmwareVersion 				= atof(fValue)												;
			else if   	(!strcmp(fName, config->KeyCode))							for (i=0;i<3;i++)		config->Code [i] 						= fValue[i]													;

			// 2. Date time info.
			else if		(!strcmp(fName, config->KeyYear))													config->nYear							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMonth))													config->nMonth							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyDay))													config->nDay							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyHour))													config->nHour							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMinute))													config->nMinute							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeySecond))													config->nSecond							= atoi(fValue)												;

			// 3. Logging info.
			else if		(!strcmp(fName, config->KeyIsRelease))												config->IsRelease						= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;
			else if		(!strcmp(fName, config->KeyIsASCII))												config->IsASCII							= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;
			else if		(!strcmp(fName, config->KeyFrequency))												config->nFrequency						= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyBufferCount))											config->nBufferCount					= MIN(MAXNBuffer, atoi(fValue))								;
			else if		(!strcmp(fName, config->KeyBufferSize))												config->nBufferSize						= MIN(MAXBufferSize, atoi(fValue))							;

			// 4. Terminal display and save file info.
			else if		(!strcmp(fName, config->KeyIsPrintResult))											config->IsPrintResult					= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;

			// 5. pig detils
			else if		(!strcmp(fName, config->KeyBodyDiameterInMM))										config->BodyDiameterInMM				= atof(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerCount))											config->nOdometerCount					= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerDiameterInMM))									config->OdometerDiameterInMM			= atof(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerType))											config->nOdometerType 					= (fValue[0]=='m'||fValue[0]=='M')? odoMagetic:
																																					  (fValue[0]=='p'||fValue[0]=='P')? odoPulse	: odoAnalog ;
			else if 	(!strcmp(fName, config->KeyOdometerPulsePerRevolution))								config->nOdometerPulsePerRevolution		= atoi(fValue)												;
 		}
		fclose (h_File);

	}
}

void parseCaliperConfiguration(char *FileName, CONFIGURATION* config)
{
	FILE *h_File ;
	char fName [80], fSymbol[10], fValue [200];

	int i;
	h_File = fopen(FileName, "r");

	if (h_File)
	{
		while (!feof(h_File))
		{
			fscanf(h_File, "%s %s %200[^\n]", fName, fSymbol, fValue);

			// 1. Version info.
			if 			(!strcmp(fName, config->KeyFirmwareVersion))										config->FirmwareVersion 				= atof(fValue)												;
			else if   	(!strcmp(fName, config->KeyCode))							for (i=0;i<3;i++)		config->Code [i] 						= fValue[i]													;

			// 2. Date time info.
			else if		(!strcmp(fName, config->KeyYear))													config->nYear							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMonth))													config->nMonth							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyDay))													config->nDay							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyHour))													config->nHour							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMinute))													config->nMinute							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeySecond))													config->nSecond							= atoi(fValue)												;

			// 3. Logging info.
			else if		(!strcmp(fName, config->KeyIsRelease))												config->IsRelease						= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;
			else if		(!strcmp(fName, config->KeyIsASCII))												config->IsASCII							= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;
			else if		(!strcmp(fName, config->KeyFrequency))												config->nFrequency						= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyBufferCount))											config->nBufferCount					= MIN(MAXNBuffer, atoi(fValue))								;
			else if		(!strcmp(fName, config->KeyBufferSize))												config->nBufferSize						= MIN(MAXBufferSize, atoi(fValue))							;

			// 4. Terminal display and save file info.
			else if		(!strcmp(fName, config->KeyIsPrintResult))											config->IsPrintResult					= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;

			// 5. pig detils
			else if		(!strcmp(fName, config->KeyBodyDiameterInMM))										config->BodyDiameterInMM				= atof(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerCount))											config->nOdometerCount					= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerDiameterInMM))									config->OdometerDiameterInMM			= atof(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerType))											config->nOdometerType 					= (fValue[0]=='m'||fValue[0]=='M')? odoMagetic:
																																					  (fValue[0]=='p'||fValue[0]=='P')? odoPulse	: odoAnalog ;
			else if 	(!strcmp(fName, config->KeyOdometerPulsePerRevolution))								config->nOdometerPulsePerRevolution		= atoi(fValue)												;

			// 6. caliper specific information
			else if		(!strcmp(fName, config->KeyCaliperArmCount))										config->nCaliperArmCount 				= atoi(fValue)												;
			else if 	(!strcmp(fName, config->KeyCaliperArmLenghtInMM))									config->CaliperArmLenghtInMM			= atof(fValue)												;
			else if 	(!strcmp(fName, config->KeyCaliperArmCenterOffsetInMM)) 							config->CaliperArmCenterOffsetInMM		= atof(fValue)												;

			// 7. MFL specific information
			else if		(!strcmp(fName, config->KeyMFLBoardCount))											config->nMFLBoardCount 					= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMFLADCChannelCount))										config->nMFLADCChannelCount 			= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMFLMUXChannelCount))										config->nMFLMUXChannelCount 			= atoi(fValue)												;

			// 8. caliper calibration information
			else if		(!strcmp(fName, config->KeyCaliperCalMaskBits))										config->nCaliperCalMaskBits 			= strtol(fValue, NULL, 16)									;
			else if		(!strcmp(fName, config->KeyCaliperCalParamsMAX))									parseMultiInt(fValue, config->nCaliperCalParamsMAX)													;
			else if		(!strcmp(fName, config->KeyCaliperCalParamsMIN))									parseMultiInt(fValue, config->nCaliperCalParamsMIN)													;
			else if		(!strcmp(fName, config->KeyCaliperCalDiaMAX))										config->nCaliperCalDiaMAX				= atof(fValue)												;
			else if		(!strcmp(fName, config->KeyCaliperCalDiaMIN))										config->nCaliperCalDiaMIN				= atof(fValue)												;

			//Extra calibration parameter parse
			PARSE_EXTPARAMS(1);
			PARSE_EXTPARAMS(2);
			PARSE_EXTPARAMS(3);
			PARSE_EXTPARAMS(4);
			PARSE_EXTPARAMS(5);
			PARSE_EXTPARAMS(6);
			PARSE_EXTPARAMS(7);
			PARSE_EXTPARAMS(8);
			PARSE_EXTPARAMS(9);
			PARSE_EXTPARAMS(10);
 		}
		fclose (h_File);

	}
}

void parseMFLConfiguration(char *FileName, CONFIGURATION* config)
{
	FILE *h_File ;
	char fName [80], fSymbol[10], fValue [200];

	int i;
	h_File = fopen(FileName, "r");

	if (h_File)
	{
		while (!feof(h_File))
		{
			fscanf(h_File, "%s %s %200[^\n]", fName, fSymbol, fValue);

			// 1. Version info.
			if 			(!strcmp(fName, config->KeyFirmwareVersion))										config->FirmwareVersion 				= atof(fValue)												;
			else if   	(!strcmp(fName, config->KeyCode))							for (i=0;i<3;i++)		config->Code [i] 						= fValue[i]													;

			// 2. Date time info.
			else if		(!strcmp(fName, config->KeyYear))													config->nYear							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMonth))													config->nMonth							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyDay))													config->nDay							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyHour))													config->nHour							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMinute))													config->nMinute							= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeySecond))													config->nSecond							= atoi(fValue)												;

			// 3. Logging info.
			else if		(!strcmp(fName, config->KeyIsRelease))												config->IsRelease						= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;
			else if		(!strcmp(fName, config->KeyIsASCII))												config->IsASCII							= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;
			else if		(!strcmp(fName, config->KeyFrequency))												config->nFrequency						= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyBufferCount))											config->nBufferCount					= MIN(MAXNBuffer, atoi(fValue))								;
			else if		(!strcmp(fName, config->KeyBufferSize))												config->nBufferSize						= MIN(MAXBufferSize, atoi(fValue))							;

			// 4. Terminal display and save file info.
			else if		(!strcmp(fName, config->KeyIsPrintResult))											config->IsPrintResult					= (fValue[0]=='Y'|| fValue[0]=='y')	? 1: 0					;

			// 5. pig detils
			else if		(!strcmp(fName, config->KeyBodyDiameterInMM))										config->BodyDiameterInMM				= atof(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerCount))											config->nOdometerCount					= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerDiameterInMM))									config->OdometerDiameterInMM			= atof(fValue)												;
			else if		(!strcmp(fName, config->KeyOdometerType))											config->nOdometerType 					= (fValue[0]=='m'||fValue[0]=='M')? odoMagetic:
																																					  (fValue[0]=='p'||fValue[0]=='P')? odoPulse	: odoAnalog ;
			else if 	(!strcmp(fName, config->KeyOdometerPulsePerRevolution))								config->nOdometerPulsePerRevolution		= atoi(fValue)												;

			// 7. MFL specific information
			else if		(!strcmp(fName, config->KeyMFLBoardCount))											config->nMFLBoardCount 					= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMFLADCChannelCount))										config->nMFLADCChannelCount 			= atoi(fValue)												;
			else if		(!strcmp(fName, config->KeyMFLMUXChannelCount))										config->nMFLMUXChannelCount 			= atoi(fValue)												;

 		}
		fclose (h_File);

	}
}

void parseConfiguration(char *FileName, CONFIGURATION* config, LoggingOperationMode device)
{
	switch (device)
	{
		case LOM_IMU:
			parseIMUConfiguration(FileName, config);
			break;

		case LOM_CALIPER:
			parseCaliperConfiguration(FileName, config);
			break;

		case LOM_MFL:
			parseMFLConfiguration(FileName, config);
			break;

		default:
			break;
	}

}
//-------------------------------------------------------------------------------------------
void  saveConfiguration(char *FileName, CONFIGURATION*config, LoggingOperationMode device)
{
	FILE *h_File ;		
			
	h_File = fopen(FileName, "w");		
		
	if (h_File)
	{	
		printConfiguration(config, h_File, device);
		fclose(h_File);
	}	
}
//-------------------------------------------------------------------------------------------
int isFileExists (char *filename)
{
 	struct stat   buffer;   
 	return (stat (filename, &buffer) == 0);
}
//-------------------------------------------------------------------------------------------
void uppercase( char *sPtr )
{
	while( *sPtr != '\0' )
	{
		*sPtr = toupper( ( unsigned char ) *sPtr );
		sPtr = sPtr +1;
	};
}
//-------------------------------------------------------------------------------------------

static void _mkdir(const char *dir)
{
        char tmp[256];
        char *p = NULL;
        size_t len;

        snprintf(tmp, sizeof(tmp),"%s",dir);
        len = strlen(tmp);
        if(tmp[len - 1] == '/')
                tmp[len - 1] = 0;
        for(p = tmp + 1; *p; p++)
                if(*p == '/') {
                        *p = 0;
                        mkdir(tmp, S_IRWXU);
                        *p = '/';
                }
        mkdir(tmp, S_IRWXU);
}

void device_config_intial(void)
{
	struct stat st = {0};

	printf("DEVICE TYPE: %s\n", DEVICE_TYPE);

	LoggingOperationMode device_type =  get_devicetype(DEVICE_TYPE);

	switch(device_type)
	{
		case LOM_IMU:

			printf("DEVICE: IMU\n");
			// Check IMU Data Folder if not exist create folder
			if (stat(IMUDataPath, &st) == -1) {
				_mkdir(IMUDataPath);
			}

			// Copy IMU directory path
			strcpy(DirPath,  cIMUDataPath);

			// Add device code
			strncpy(m_Config.Code, device_code[LOM_IMU], 3);

			break;

		case LOM_CALIPER:

			printf("DEVICE: CALIPER\n");
			// Check Caliper Data Folder if not exist create folder
			if (stat(CaliperDataPath, &st) == -1) {
				_mkdir(CaliperDataPath);
			}

			// Copy Caliper directory path
			strcpy(DirPath,  cCaliperDataPath);

			// Add device code
			strncpy(m_Config.Code, device_code[LOM_CALIPER], 3);

			break;

		case LOM_MFL:

			printf("DEVICE: MFL\n");
			// Check MFL Data Folder if not exist create folder
			if (stat(MFLDataPath, &st) == -1) {
				_mkdir(MFLDataPath);
			}

			// Copy MFL directory path
			strcpy(DirPath,  cMFLDataPath);

			// Add device code
			strncpy(m_Config.Code, device_code[LOM_MFL], 3);

			break;

		default:

			printf("DEVICE: UNKNOWN\n");
			// Check out of scope Data Folder if not exist create folder
			if (stat(DefaultDataPath, &st) == -1) {
				_mkdir(DefaultDataPath);
			}

			// Copy default directory path
			strcpy(DirPath,  cMFLDataPath);

			break;
	}

	// Load current configuration for config file
	strcpy(ConfigPath, DirPath);
	strcat(ConfigPath, cConfigFileName);
	printf("ConfigPath: %s\n", ConfigPath);

	ConfigFileName = ConfigPath;

	_LoadConfiguration(ConfigPath, &m_Config, device_type);
}

void device_config_reintial(void)
{
	printf("Device config reinitial\n");

	LoggingOperationMode device_type =  get_devicetype(DEVICE_TYPE);

	_LoadConfiguration(ConfigPath, &m_Config, device_type);
}


