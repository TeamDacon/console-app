// tcpClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "tcp_cmd_caliper.h"
#include "tcp_cmd_common.h"
#define WIN32_LEAN_AND_MEAN
#define FWVersion 1.3

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <typeinfo>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "Mswsock.lib")
#pragma comment(lib, "AdvApi32.lib")

#define DEFAULT_BUFLEN 4096 * 8
#define DEFAULT_PORT "1500"
#define MAX_MSG 10000
#define MaxNArms 22

void printDaconASCII(void)
{
    printf("\n");
    printf("\t _____\n");
    printf("\t |  __ \\ \n");
    printf("\t | |  | | __ _  ___ ___  _ __ \n");
    printf("\t | |  | |/ _` |/ __/ _ \\| '_ \\   \n");
    printf("\t | |__| | (_| | (_| (_) | | | |  \n");
    printf("\t |_____/ \\__,_|\\___\\___/|_| |_|   _   _           \n");
    printf("\t |_   _|                         | | (_)            \n");
    printf("\t   | |  _ __  ___ _ __   ___  ___| |_ _  ___  _ __  \n");
    printf("\t   | | | '_ \\/ __| '_ \\ / _ \\/ __| __| |/ _ \\| '_ \\ \n");
    printf("\t  _| |_| | | \\__ \\ |_) |  __/ (__| |_| | (_) | | | |\n");
    printf("\t |_____|_| |_|___/ .__/ \\___|\\___|\\__|_|\\___/|_| |_|\n");
    printf("\t  / ____|        | |    (_)                         \n");
    printf("\t | (___   ___ _ _|_|   ___  ___ ___  ___            \n");
    printf("\t  \\___ \\ / _ \\ '__\\ \\ / / |/ __/ _ \\/ __|           \n");
    printf("\t  ____) |  __/ |   \\ V /| | (_|  __/\\__ \\           \n");
    printf("\t |_____/ \\___|_|    \\_/ |_|\\___\\___||___/          \n");
    printf("\n");
    printf("\n");
}

// This function print the available choices of the user from the last state of the recording/process
void printChoices(int choice)
{
    if (choice == 0) {
        printf("\t+-----------------------------------------------------+\n");
        printf("\t|-----------------------------------------------------|\n");
        printf("\t||                                                   ||\n");
        printf("\t||   Console Application to interact with the board  ||\n");
        printf("\t||   Please Select a command :                       ||\n");
        printf("\t||                                                   ||\n");
        printf("\t||      1. Stop recording                            ||\n");
        printf("\t||      2. Restart the record                        ||\n");
        printf("\t||      3. Kill the process                          ||\n");
        printf("\t||  Press 'q' to quit this program                   ||\n");
        printf("\t+-----------------------------------------------------+\n");
    }
    if (choice == 1) {
        printf("\t+-----------------------------------------------------+\n");
        printf("\t|-----------------------------------------------------|\n");
        printf("\t||                                                   ||\n");
        printf("\t||   Console Application to interact with the board  ||\n");
        printf("\t||   Please Select a command :                       ||\n");
        printf("\t||                                                   ||\n");
        printf("\t||      2. Restart the record                        ||\n");
        printf("\t||      3. Kill the process                          ||\n");
        printf("\t||  Press 'q' to quit this program                   ||\n");
        printf("\t+-----------------------------------------------------+\n");
    }
    if (choice == 2) {
        printf("\t+-----------------------------------------------------+\n");
        printf("\t|-----------------------------------------------------|\n");
        printf("\t||                                                   ||\n");
        printf("\t||   Console Application to interact with the board  ||\n");
        printf("\t||   Please Select a command :                       ||\n");
        printf("\t||                                                   ||\n");
        printf("\t||      1. Stop recording                            ||\n");
        printf("\t||      2. Restart the record                        ||\n");
        printf("\t||      3. Kill the process                          ||\n");
        printf("\t||  Press 'q' to quit this program                   ||\n");
        printf("\t+-----------------------------------------------------+\n");
    }
    if (choice == 3) {
        printf("\t+-----------------------------------------------------+\n");
        printf("\t|-----------------------------------------------------|\n");
        printf("\t||                                                   ||\n");
        printf("\t||   Console Application to interact with the board  ||\n");
        printf("\t||   Please Select a command :                       ||\n");
        printf("\t||                                                   ||\n");
        printf("\t||      4. Start the process                         ||\n");
        printf("\t||  Press 'q' to quit this program                   ||\n");
        printf("\t+-----------------------------------------------------+\n");
    }

    if (choice == 4) {
        printf("\t+-----------------------------------------------------+\n");
        printf("\t|-----------------------------------------------------|\n");
        printf("\t||                                                   ||\n");
        printf("\t||   Console Application to interact with the board  ||\n");
        printf("\t||   Please Select a command :                       ||\n");
        printf("\t||                                                   ||\n");
        printf("\t||      1. Stop recording                            ||\n");
        printf("\t||      2. Restart the record                        ||\n");
        printf("\t||      3. Kill the process                          ||\n");
        printf("\t||  Press 'q' to quit this program                   ||\n");
        printf("\t+-----------------------------------------------------+\n");
    }
}

// int _tmain(int argc, _TCHAR* argv[])
int __cdecl main(int argc, char** argv)
{
    TCP_PACKET_CAL SendPacket;
    TCP_PACKET_CAL RecvPacket;
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    size_t data_length = 0;
    static int rcv_ptr = 0;
    static char rcv_msg[MAX_MSG];
    static int ret;
    struct addrinfo *result = NULL, *ptr = NULL, hints;
    char recvbuf[DEFAULT_BUFLEN];
    char userCMD, prev_userCMD;
    int iResult, scanned;
    int recvbuflen = DEFAULT_BUFLEN;
    char killLogging[1000];

    // Validate the parameters
    if (argc != 2) {
        printf("usage: %s server-name\n", argv[0]);
        return 1;
    }

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Unable to connect to the board!\n\n");
        printf("Please check that the address you entered is correct,\n");
        printf("And that the Data logger is running on board.\n");
        WSACleanup();
        return 1;
    }
	// We are connected to the board
	// We print the landing ASCII Dacon design as well as the options available
    printDaconASCII();
    printChoices(0);
	// Initialize the previous state as if a Restart
    prev_userCMD = '2';

	// Endless loop will be exited only when the user enters 'q' to quit the program
    while (1) {
        printf("          ");
        scanned = scanf_s("%c", &userCMD, 1);
        scanf_s("%*[^\n]");
        getchar();
		// While the user doesn't provide a valid command (1,2,3,4 or q) we keep asking him
        while ((scanned != 1) || ((userCMD != '1') && (userCMD != '2') && (userCMD != '3') && (userCMD != 'q') && (userCMD != '4'))) {
            printf("          Invalid parameter\n");
            printf("          ");
            scanned = scanf_s("%c", &userCMD, 1);
            scanf_s("%*[^\n]");
            getchar();
        }

		// We check that the user enter a correct command according to the last state the board was in
        if (prev_userCMD == '1') {
            while ((scanned != 1) || ((userCMD != '2') && (userCMD != '3') && (userCMD != 'q'))) {
                printf("          Not allowed in this configuration\n");
                printf("          ");
                scanned = scanf_s("%c", &userCMD, 1);
                scanf_s("%*[^\n]");
                getchar();
            }
        }

        if ((prev_userCMD == '2') || (prev_userCMD == '4')) {
            while ((scanned != 1) || ((userCMD != '1') && (userCMD != '2') && (userCMD != '3') && (userCMD != 'q'))) {
                printf("          Not allowed in this configuration\n");
                printf("          ");
                scanned = scanf_s("%c", &userCMD, 1);
                scanf_s("%*[^\n]");
                getchar();
            }
        }

        if (prev_userCMD == '3') {
            while ((scanned != 1) || ((userCMD != '4') && (userCMD != 'q'))) {
                printf("          Not allowed in this configuration\n");
                printf("          ");
                scanned = scanf_s("%c", &userCMD, 1);
                scanf_s("%*[^\n]");
                getchar();
            }
        }

        // Save the configuration
        prev_userCMD = userCMD;

		// From here we know the user entered a correct command, so we can apply it

		// The user wish to Stop the recording
        if (userCMD == '1') {
            // StopRecord
            memset((void*)&SendPacket, 0x0, sizeof(&SendPacket));
            SendPacket.PacketInfo.PacketID = SetCaliperStartStopRecordCommand;
            data_length += sizeof(SendPacket.PacketInfo);

            // Setup Header Sub-Info
            SendPacket.PacketSubInfo.FirmwareVer = (float)FWVersion;
            data_length += sizeof(SendPacket.PacketSubInfo);

            // Build the Packet data to send, a 0 here means the SetCaliperStartStopRecordCommand is for Stop
            SendPacket.AppData.Data[0] = 0;

            data_length += sizeof(SendPacket.AppData.caliper_params_cal);

            // Setup Packet Size
            SendPacket.PacketInfo.DataLength = sizeof(SendPacket.AppData);

            iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
            if (iResult == SOCKET_ERROR) {
                printf("          ");
                printf("send failed with error: %ld\n", WSAGetLastError());
                closesocket(ConnectSocket);
                WSACleanup();
                return 1;
            }

            memset(recvbuf, 0x0, DEFAULT_BUFLEN);
            iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
            memcpy((void*)&RecvPacket, recvbuf, iResult);
            if (RecvPacket.PacketInfo.PacketID != SetCaliperStartStopRecordResponse) {
                printf("          ");
                printf("SetCaliperStartStopRecord error\n");
            }
            printf("          ");
            printf("Record stopped !\n");
        }

        else {
			// The user wish to Restart the recording ( stop and then start a new record )
            if (userCMD == '2') {
                // StopRecord
                memset((void*)&SendPacket, 0x0, sizeof(&SendPacket));
                SendPacket.PacketInfo.PacketID = SetCaliperStartStopRecordCommand;
                data_length += sizeof(SendPacket.PacketInfo);

                // Setup Header Sub-Info
                SendPacket.PacketSubInfo.FirmwareVer = (float)FWVersion;
                data_length += sizeof(SendPacket.PacketSubInfo);

				// Build the Packet data to send, a 0 here means the SetCaliperStartStopRecordCommand is for Stop
                SendPacket.AppData.Data[0] = 0;

                data_length += sizeof(SendPacket.AppData.caliper_params_cal);

                // Setup Packet Size
                SendPacket.PacketInfo.DataLength = sizeof(SendPacket.AppData);

                iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
                if (iResult == SOCKET_ERROR) {
                    printf("          ");
                    printf("send failed with error: %ld\n", WSAGetLastError());
                    closesocket(ConnectSocket);
                    WSACleanup();
                    return 1;
                }

                memset(recvbuf, 0x0, DEFAULT_BUFLEN);
                iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
                memcpy((void*)&RecvPacket, recvbuf, iResult);
                if (RecvPacket.PacketInfo.PacketID != SetCaliperStartStopRecordResponse) {
                    printf("          ");
                    printf("SetCaliperStartStopRecord error\n");
                }

                // StartRecord
                memset((void*)&SendPacket, 0x0, sizeof(&SendPacket));
                SendPacket.PacketInfo.PacketID = SetCaliperStartStopRecordCommand;
                data_length += sizeof(SendPacket.PacketInfo);

                // Setup Header Sub-Info
                SendPacket.PacketSubInfo.FirmwareVer = (float)FWVersion;
                data_length += sizeof(SendPacket.PacketSubInfo);

				// Build the Packet data to send, a 1 here means the SetCaliperStartStopRecordCommand is for Start
                SendPacket.AppData.Data[0] = 1;

                data_length += sizeof(SendPacket.AppData.caliper_params_cal);

                // Setup Packet Size
                SendPacket.PacketInfo.DataLength = sizeof(SendPacket.AppData);

                iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
                if (iResult == SOCKET_ERROR) {
                    printf("          ");
                    printf("send failed with error: %d\n", WSAGetLastError());
                    closesocket(ConnectSocket);
                    WSACleanup();
                    return 1;
                }

                memset(recvbuf, 0x0, DEFAULT_BUFLEN);
                iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
                memcpy((void*)&RecvPacket, recvbuf, iResult);
                if (RecvPacket.PacketInfo.PacketID != SetCaliperStartStopRecordResponse) {
                    printf("          ");
                    printf("SetCaliperStartStopRecord error\n");
                }
                printf("          ");
                printf("Record restarted !\n");
            }

            else {
				// The user wish to kill the DaconLogging process
                if (userCMD == '3') {
                    strcpy_s(killLogging, "plink -l root -pw acmesystems root@");
                    strcat_s(killLogging, argv[1]);
                    strcat_s(killLogging, " /media/data/caliper_code/killDaconLogging.sh");
                    system(killLogging);
                    printf("          ");
                    printf("Dacon Logging has been killed !\n");
                }

                else {
					// The user wish to launch the DaconLogging process
                    if (userCMD == '4') {
                        strcpy_s(killLogging, "start plink -l root -pw acmesystems root@");
                        strcat_s(killLogging, argv[1]);
                        strcat_s(killLogging, " /media/data/caliper_code/DaconLogging");
                        system(killLogging);
                        printf("          ");
                        printf("Dacon Logging has been launched !\n");
                    } else {
						// The user wish to exit this console App

                        // shutdown the connection since no more data will be sent
                        iResult = shutdown(ConnectSocket, SD_SEND);
                        if (iResult == SOCKET_ERROR) {
                            printf("          ");
                            printf("shutdown failed with error: %d\n", WSAGetLastError());
                            closesocket(ConnectSocket);
                            WSACleanup();
                            return 1;
                        }
                        // cleanup
                        closesocket(ConnectSocket);
                        WSACleanup();
                        printf("          ");
                        printf("Closed !");
                        return 0;
                    }
                }
            }
        }
		// We call the function to print the choices with the current state as parameter
        printChoices((int)userCMD - '0');
    }
    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        printf("          ");
        printf("shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }
    // cleanup
    closesocket(ConnectSocket);
    WSACleanup();

    return 0;
}
