#include <Windows.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <tchar.h>
#include <typeinfo>
using namespace std;
#define MAXDEVICES	100

string ExePath() {
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer).substr(0, pos);
}

int main(void)
{
	string launch;
	string importPuTTY = "reg import putty_sessions.reg";
	LONG lResult;
	HKEY hKey;
	// Need to find if HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\Sessions\clock exits, if it doesn't install it from putty_session

	lResult = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT("Software\\SimonTatham\\PuTTY\\Sessions\\clock"), 0, KEY_READ, &hKey);

	if (lResult != ERROR_SUCCESS)
	{
		if (lResult == ERROR_FILE_NOT_FOUND) {
			system(importPuTTY.c_str());
			printf("Importing Key\n");
		}
		else {
			printf("Error opening key.\n");
			return FALSE;
		}
	}
	printf("Key found\n");

	launch = "putty.exe -load \"clock\" root@192.168.10.10 -pw acmesystems -m \"command.txt\"";

	system(launch.c_str());
	
	return 0;
}