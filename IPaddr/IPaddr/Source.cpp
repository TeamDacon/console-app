#include <Windows.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <typeinfo>
using namespace std;
#define MAXDEVICES	100

void printChoices(void)
{
    printf("\t+-----------------------------------------------------+\n");
    printf("\t|-----------------------------------------------------|\n");
    printf("\t||                                                   ||\n");
    printf("\t||   Setting IP adress of the tool.                  ||\n");
    printf("\t||   Please Select the kind of tool :                ||\n");
    printf("\t||                                                   ||\n");
    printf("\t||      1. Caliper                                   ||\n");
    printf("\t||      2. MFL                                       ||\n");
    printf("\t||      3. IMU                                       ||\n");
    printf("\t||  Press 'q' to quit this program                   ||\n");
    printf("\t+-----------------------------------------------------+\n");
}

int main(void)
{
    int scanned, userIP, i = 0;
    char userCMD;
    string command;
    string reboot;
    string commandIP;
    string storeKey;
    string arpScanLines[100];
    char psBuffer[128];
	int coma = 0;
	string arpcommand, parseDataNumber, dowloadData, path;
	FILE *fp;
	char addrDevice[MAXDEVICES][16];
	int j = 0;
	int max = 0;
	int nbDevices = 0;

    printChoices();
    printf("          ");
    scanned = scanf_s("%c", &userCMD, 1);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || ((userCMD != '1') && (userCMD != '2') && (userCMD != '3') && (userCMD != 'q'))) 
	{
        printf("          Invalid parameter\n");
        printf("          ");
        scanned = scanf_s("%c", &userCMD, 1);
        scanf_s("%*[^\n]");
        getchar();
    }

	if (userCMD == 'q')
	{
		printf("          Program closed.\n");
		system("pause");
		return 0;
	}

	// Scanning Ips
	arpcommand = "arp -a | findstr ";
	arpcommand += '"';
	arpcommand += "192.168.10.1 192.168.20.1 192.168.10.200";//PROBLEM 10.20 and 20.20 CAN DECTECT 20 PC !!!
	arpcommand += '"';
	if ((fp = _popen(arpcommand.c_str(), "rt")) == NULL)
		exit(1);
	while (fgets(psBuffer, 128, fp))
	{
		arpScanLines[i++] = psBuffer;
	};
	_pclose(fp);
	i = 0;
	while (arpScanLines[i] != "\0")
	{
		arpScanLines[i] = strstr(arpScanLines[i].c_str(), "192.168.");
		i++;
	}

	i = 0;
	while (arpScanLines[i] != "\0")
	{
		if (userCMD == '1') strncpy_s(addrDevice[nbDevices], arpScanLines[i].c_str(), 13);
		else strncpy_s(addrDevice[nbDevices], arpScanLines[i].c_str(), 14);
		i++;
		nbDevices++;
	}

	if (nbDevices == 0)
	{
		printf("Could not detect any device\n");
		return -1;
	}
	else
	{
		if (nbDevices > 1)
		{
			printf("Several devices connected\n");
			printf("Please connect only one and restart!\n");
			return -1;
		}
	}

	cout << "\n\tDetected device : \n\n";

	cout << "\t" << addrDevice[0] << "\n";
	cout << "\n";

    printf("          Please type the 2 last digits of your wished IP :\n");
    printf("          ");
    scanned = scanf_s("%d", &userIP);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (userIP < 0) || (userIP > 55) || (userIP == 20)) 
	{
        printf("          Invalid parameter\n");
        printf("          ");
        scanned = scanf_s("%d", &userIP);
        scanf_s("%*[^\n]");
        getchar();
    }
    commandIP = "plink -l root -pw acmesystems root@";
    commandIP += addrDevice[0];
    reboot = commandIP;
	commandIP += " sed -i 's/address ";
	commandIP += addrDevice[0];
	if (userCMD == '1')
		commandIP += "/address 192.168.10.";
    else 
	{
        if (userCMD == '2')
            commandIP += "/address 192.168.20.1";
        else 
		{
            if (userCMD == '3')
                commandIP += "/address 192.168.10.2";
        }
    }

    commandIP += (userIP / 10) + '0';
    commandIP += (userIP % 10) + '0';
    commandIP += "/g' /etc/network/interfaces > NUL";
    WinExec(commandIP.c_str(), SW_HIDE);
    reboot += " systemctl -ff";
    WinExec(reboot.c_str(), SW_HIDE);
    Sleep(4000);
    system("taskkill /F /IM plink.exe > NUL");
    printf("\n          The board is rebooting with the new IP address :\n");
	if (userCMD == '1')
	{
		printf("          192.168.10.%02d\n", userIP);
	}
    else 
	{
        if (userCMD == '2')
            printf("          192.168.20.1%02d\n", userIP);
        else {
            if (userCMD == '3')
                printf("          192.168.10.2%02d\n", userIP);
        }
    } 
	system("pause");
    return 0;
}
