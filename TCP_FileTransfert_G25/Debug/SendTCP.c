/* gcc SendTCP.c -Wall -pedantic -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64 -o sendtcp */

#define __USE_LARGEFILE64
#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#define CALIPER	0
#define IMU	1

int main(int argc , char *argv[])
{
	int socket_desc;
	struct sockaddr_in server;
	int fd;
	int read_len;
	char datanb[1];
	char  FileToDL[1000]	="\0"  	;
	char  PathData[1000] 	="\0"	;
	struct stat64 myst;
	int x;
	int synRetries = 2;
	int device = 0;
	sleep(4);
	
	sprintf(PathData,"/media/data/CaliperDataFiles");	
	if (access(PathData, F_OK) != -1)
	{
		device = CALIPER;
	}
	else
	{		
	sprintf(PathData,"/media/data/IMUDataFiles");	
	if (access(PathData, F_OK) != -1)
		{
			device = IMU;
		}
	}	
	/*Create socket */
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}
		
	server.sin_addr.s_addr = inet_addr("192.168.10.20");
	server.sin_family = AF_INET;
	server.sin_port = htons( 22 ); /* Port number 22 */
	
	/* Send a total of 3 SYN packets => Timeout ~7s */
	setsockopt(socket_desc, IPPROTO_TCP, TCP_SYNCNT, &synRetries, sizeof(synRetries));

	/*Connect to remote server*/
	if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		puts("connect error");
		return 1;
	}
	/* receive the data number (DataXXX.imu) */
	recv(socket_desc, datanb, sizeof(datanb), 0);
	if(device == 0) sprintf(FileToDL,"/media/data/CaliperDataFiles/Data%03d.CAL",datanb[0]);
	else sprintf(FileToDL,"/media/data/IMUDataFiles/Data%03d.imu",datanb[0]);

	fd = open(FileToDL, O_RDONLY);

/* Look at the size of the wanted Data file */
 x = stat64(FileToDL, &myst );
if ( x < 0 )
{
	/* The file does not exist/cannot be opened */
    printf("x is %d\n", x);
	myst.st_size = 0;
	/* Send a size of 0 byte */
	send(socket_desc, &myst.st_size, sizeof(myst.st_size), 0);
    perror("stat failure");
}
else
{
/* Send the the of the wanted Data file, in byte */
send(socket_desc, &myst.st_size, sizeof(myst.st_size), 0);
}
	if (fd == -1) return 0;

/* If the file is smaller than 2 GB (so the size is smaller than the limit of int in Byte 2147483647) */
if(myst.st_size <=  2147482624) read_len = sendfile(socket_desc, fd, NULL, myst.st_size);
else
{
	/* If the file is bigger than 2 GB (so the size is bigger than the limit of int in Byte 2147483647) */
	/* it cannot be passed as a buffer size to the function sendfile(), so we split in several sendfile() functions */
	read_len = sendfile(socket_desc, fd, NULL, 2147482624);
	while(read_len != 0) 	read_len = sendfile(socket_desc, fd, NULL, 2147482624);
}

	return 0;
}