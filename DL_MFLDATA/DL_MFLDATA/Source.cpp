#include <Windows.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <tchar.h>
#include <typeinfo>
#define MAXCRABBOX	100
using namespace std;

int main(void)
{

	string arpcommand, parseDataNumber, dowloadData, path;
	FILE *fp;
	char psBuffer[128];
	char addrDevice[MAXCRABBOX][15];
	string arpScanLines[100], ScanDataFiles[200];
	int i = 0;
	int j = 0;
	int max = 0;
	int nbCrabBox = 0;
	/* Open the command for reading. */
	arpcommand = "arp -a | findstr ";
	arpcommand += '"';
	arpcommand += "192.168.20.1";
	arpcommand += '"';
	if ((fp = _popen(arpcommand.c_str(), "rt")) == NULL)
		exit(1);
	while (fgets(psBuffer, 128, fp))
	{
		arpScanLines[i++] = psBuffer;
	};
	_pclose(fp);
	i = 0;
	while (arpScanLines[i] != "\0")
	{
		arpScanLines[i] = strstr(arpScanLines[i].c_str(), "192.168.20.1");
		i++;
	}

	i = 0;
	while (arpScanLines[i] != "\0")
	{
		arpScanLines[i] = strstr(arpScanLines[i].c_str(), "192.168.20.1");
		strncpy_s(addrDevice[nbCrabBox], arpScanLines[i].c_str(), 14);
		i++;
		nbCrabBox++;
	}

	/* close */

	cout << "\n\tDetected CrabBox : \n\n";
	for (i = 0; i < nbCrabBox; i++)
	{
		cout << "\t" <<addrDevice[i] <<"\n";
	}

	i = 0;
	// Parse GeckoXX_nb.mfl to download the previous number
	parseDataNumber = "plink -t -l root -pw acmesystems root@";
	parseDataNumber += addrDevice[0];
	parseDataNumber += " ls /media/data/GeckoDataFiles/ -1";
	if ((fp = _popen(parseDataNumber.c_str(), "rt")) == NULL)
		exit(1);
	while (fgets(psBuffer, 128, fp))
	{
		ScanDataFiles[i++] = psBuffer;
	};
	_pclose(fp);

	i = 0;
	while (ScanDataFiles[i] != "\0")
	{
		if (stoi(ScanDataFiles[i].substr(8, 3)) > max)
		{
			max = stoi(ScanDataFiles[i].substr(8, 3));
		}
		i++;
	}

	// Download the data of all CraBBox
	cout << "\n\tDownloading data...\n";
	for (i = 0; i < nbCrabBox; i++)
	{
		dowloadData = "cd DataFolder && curl -u root:acmesystems ";
		dowloadData += '"';
		dowloadData += "ftp://";
		dowloadData += addrDevice[i];
		if (i + 1 < 10) dowloadData += "/Gecko0" + to_string(i + 1);
		else dowloadData += "/Gecko" + to_string(i + 1);
		if (max - 1 < 10)
		{
			dowloadData += "_00";
		}
		else
		{
			if (max - 1 < 100)
			{
				dowloadData += "_0";
			}
			else dowloadData += "_";
		}
		dowloadData += to_string(max - 1) + ".mfl";
		dowloadData += '"';
		dowloadData += " -o Gecko" + to_string(i + 1) + "_" + to_string(max - 1) + ".mfl";
		system(dowloadData.c_str());
	}
	
	printf("          Closing\n");
	Sleep(2000);
	return 0;
}